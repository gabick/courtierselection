<?php
function my_scripts_method() {
    wp_enqueue_script(
        'custom-script',
        get_stylesheet_directory_uri() . '/js/custom-script.js',
        array( 'jquery' ),
		true
    );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method', 15 );

function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('post'));
    }

	return $query;
}

add_filter('pre_get_posts','searchfilter');

/*********************************/
/* Change Search Button Text
/**************************************/

function my_search_form_text($text) {
     $text = str_replace('value="Search"', 'value="Recherche"', $text); //set as value the text you want
     return $text;
}
add_filter('get_search_form', 'my_search_form_text');
define( 'DDPL_DOMAIN', 'my-domain' ); // translation domain
require_once( 'vendor/divi-disable-premade-layouts/divi-disable-premade-layouts.php' );

add_filter('tiny_mce_before_init', 'ag_tinymce_paste_as_text');
function ag_tinymce_paste_as_text( $init ) {
	$init['paste_as_text'] = true;
	return $init;
}
?>