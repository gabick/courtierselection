var $j = jQuery.noConflict();
const URL = location.host;
const CATEGORY_SLUG = 'category'
$j(document).ready(function(){
    $j(function(){
        /* Ã‰tat actif de la navigation selon le scroll (custom) */
        if ($j("body").hasClass("home")) {
            detectHomePage();
            $j(document).on("scroll", onScroll);
        }
        returnCategoryTitle();
        footerCopyright();

    });

    function detectHomePage() {
        var pathName = location.pathname;
        if (pathName == '/') {
            $j('.nav li').removeClass("current-menu-item");
            $j('.nav li:first').addClass("current-menu-item");
        }
    }

    // DOM des Ã©lÃ©ments du menu pour optimiser les performances
    var desktopMenuItems = $j('.nav a[href*="#"]');

    /**
     * Fonction appelÃ©e Ã  chaque event scroll pour ajouter/retirer la classe active sur les Ã©lÃ©ments du menu desktop
     *
     * @param e event
     * @return void
     */
    function onScroll(e) {
        var scrollPos = $j(document).scrollTop();
        $j(desktopMenuItems).each(function( index ) {
            var currLink = $j(this);
            var target = currLink.attr('href');
            var target = target.split('#');
            var targetLastPart = target[target.length-1];
            var targetRef = $j('#'+targetLastPart)

            if (targetRef.position().top <= scrollPos + $j(window).height() / 2 && targetRef.position().top + targetRef.height() / 1.25 > scrollPos) {
                currLink.closest('li').addClass("current-menu-item");
            } else if (index == desktopMenuItems.length -1 && scrollPos > targetRef.position().top) {
                // do nothing
            } else {
                currLink.closest('li').removeClass("current-menu-item");
            }
        });
    }
    function returnCategoryTitle() {
        var pathName = location.pathname;
        if (pathName.indexOf(CATEGORY_SLUG) >= 0) {
            var titleArray = pathName.split('/');
            // -2 because of index 0 of array
            var title = titleArray[titleArray.length-2];
            var title = title.replace(/-/g, " ");
            var stringToAdd = '<h1 style="text-align: center;">'+title+'</h1>'
            var element = $j('.heading-category .et_pb_text_inner').html(stringToAdd);

        }
    }

    function footerCopyright() {
        var $footer = $j("#footer-info")
        var $date = new Date().getFullYear();
        $footer.append('<span class="copyright">'+'\u00A9'+' '+$date+'</span>')
    }
	
	$j('#formulaire form').submit(function( event ) {
	  dataLayer.push({'event': 'formSubmitted'});
	});
})